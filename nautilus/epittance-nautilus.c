/*
 *  epittance-nautilus.c - EPittance nautilus integration
 * 
 *  Authors: Christian Kellner <gicmo@gnome-de.org>
 *           Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 * 
 */
 

#include <config.h>

#include <bonobo-activation/bonobo-activation-activate.h>

#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-column-provider.h>
#include <libnautilus-extension/nautilus-file-info.h>
#include <libnautilus-extension/nautilus-info-provider.h>
#include <libnautilus-extension/nautilus-menu-provider.h>
#include <libnautilus-extension/nautilus-property-page-provider.h>

#include <gconf/gconf-client.h>
#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <string.h>

#include <EPittance.h>

#define EPITTANCE_TYPE_NAUTILUS  (epittance_nautilus_get_type ())
#define EPITTANCE_NAUTILUS(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), EPITTANCE_TYPE_NAUTILUS, EPittanceNautilus))

typedef struct {
  GObject parent_slot;
} EPittanceNautilus;

typedef struct {
  GObjectClass parent_slot;
} EPittanceNautilusClass;

static GType epittance_nautilus_get_type (void);
static void epittance_nautilus_register_type (GTypeModule * module);

static GObjectClass * parent_class = NULL;

typedef enum {
  EPITTANCE_NOT_SHARED = 0,
  EPITTANCE_SHARED_RONLY,
  EPITTANCE_SHARED_RW
} EPittanceShareStatus;

static EPittanceShareStatus file_get_share_status (NautilusFileInfo * file) {
  GConfClient * client;
  GSList * folders;
  gboolean in_list = FALSE;
  gchar * filename, * uri;
  
  uri = nautilus_file_info_get_uri (file);
  
  if (!strncmp (uri, "file://", 7)) {
    filename = g_strdup (uri + 7);

    client = gconf_client_get_default ();
    folders = gconf_client_get_list (client,
				     "/desktop/gnome/file-sharing/folders",
				     GCONF_VALUE_STRING, NULL);

    for (; folders != NULL; folders = folders->next) {
      if (!strcmp (folders->data, filename))
	in_list = TRUE;
    }

    g_object_unref (client);
    g_free (filename);
  }
  g_free (uri);

  if (in_list)
    return EPITTANCE_SHARED_RONLY;
  else
    return EPITTANCE_NOT_SHARED;
}

/****************************************************************************/
/* property page provider */

static void epittance_sharing_toggled (GtkToggleButton * button,
				       NautilusFileInfo * file) {
  GConfClient * client;
  GSList * folders, * foo;
  gchar * filename, * uri, * bar;
  gboolean in_list = FALSE;

  uri = nautilus_file_info_get_uri (file);

  if (!strncmp (uri, "file://", 7)) {
    filename = g_strdup (uri + 7);

    client = gconf_client_get_default ();
    folders = gconf_client_get_list (client,
				     "/desktop/gnome/file-sharing/folders",
				     GCONF_VALUE_STRING, NULL);

    if (button->active) {
      for (foo = folders; foo != NULL; foo = foo->next) {
	if (!strcmp (foo->data, filename))
	  in_list = TRUE;
      }
      if (!in_list)
	folders = g_slist_append (folders, filename);
    } else {
      for (foo = folders; foo != NULL; foo = foo->next) {
	if (!strcmp (foo->data, filename)) {
	  bar = foo->data;
	  in_list = TRUE;
	}
      }
      if (in_list) {
	folders = g_slist_remove (folders, bar);
	g_free (bar);
      }
    }

    gconf_client_set_list (client, "/desktop/gnome/file-sharing/folders",
			   GCONF_VALUE_STRING, folders, NULL);

    gconf_client_suggest_sync (client, NULL);
    nautilus_file_info_invalidate_extension_info (file);

    g_object_unref (client);
    g_free (filename);
  }
  g_free (uri);
}

static GtkWidget * create_property_page (NautilusFileInfo * file) {
  GtkWidget * page, * vbox, * hbox;
  GtkWidget * label, * check;
  gchar * str;

  page = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (page), 12);

  vbox = gtk_vbox_new (FALSE, 12);
  gtk_container_add (GTK_CONTAINER (page), vbox);

  str = g_strdup_printf ("<b>%s</b>", _("Access Privileges"));
  label = gtk_label_new (str);
  g_free (str);

  gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
  
  hbox = gtk_hbox_new (FALSE, 12);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
  
  label = gtk_label_new ("");
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
  
  check = gtk_check_button_new_with_label (_("Share this folder"));
  gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 0);

  if (file_get_share_status (file) == EPITTANCE_SHARED_RONLY)
    GTK_TOGGLE_BUTTON (check)->active = TRUE;

  g_signal_connect (G_OBJECT (check), "toggled",
		    G_CALLBACK (epittance_sharing_toggled), file);

  gtk_widget_show_all (page);
  
  return page;
}


static GList * epittance_nautilus_get_property_pages (NautilusPropertyPageProvider * provider,
						      GList * files) {
  GList * pages;
  NautilusPropertyPage * page;
  NautilusFileInfo * file;
  
  /* Only show the property page if 1 file is selected */
  if (!files || files->next != NULL) {
    return NULL;
  }

  pages = NULL;
  
  file = NAUTILUS_FILE_INFO (files->data);

  if (!nautilus_file_info_is_directory (file))
    return NULL;

  page = nautilus_property_page_new ("EPittanceNautilus::property_page", 
				     gtk_label_new (_("Sharing")),
				     create_property_page (file));
  
  pages = g_list_append (pages, page);
  
  return pages;
}

static void  epittance_nautilus_property_page_provider_iface_init (NautilusPropertyPageProviderIface * iface) {
  iface->get_pages = epittance_nautilus_get_property_pages;
}

/****************************************************************************/
/* Update file info provider */
static NautilusOperationResult epittance_nautilus_update_file_info (NautilusInfoProvider * provider,
								    NautilusFileInfo * file,
								    GClosure * update_complete,
								    NautilusOperationHandle ** handle) {
  gchar *share_status = NULL;
	
  switch (file_get_share_status (file)) {
    
  case EPITTANCE_SHARED_RONLY:
    nautilus_file_info_add_emblem (file, "shared");
    share_status = _("shared (read only)");
    break;
    
  case EPITTANCE_SHARED_RW:
    nautilus_file_info_add_emblem (file, "shared");
    share_status = _("shared (read and write)");
    break;
    
  case EPITTANCE_NOT_SHARED:
    share_status = _("not shared");
    break;
    
  default:
    g_assert_not_reached ();
    break;
  }
  
  nautilus_file_info_add_string_attribute (file, 
					   "EPittanceNautilus::share_status", 
					   share_status);
  return NAUTILUS_OPERATION_COMPLETE;
}


static void epittance_nautilus_info_provider_iface_init (NautilusInfoProviderIface * iface) {
  iface->update_file_info = epittance_nautilus_update_file_info;
}


/****************************************************************************/
/* Colum provider */
static GList * epittance_nautilus_get_columns (NautilusColumnProvider * provider) {
  NautilusColumn *column;
  
  column = nautilus_column_new ("EPittanceNautilus::share_status_column",
				"EPittanceNautilus::share_status",
				_("Shared"),
				_("Share status of the directory"));
  
  return g_list_append (NULL, column);
}

static void epittance_nautilus_column_provider_iface_init (NautilusColumnProviderIface * iface) {
  iface->get_columns = epittance_nautilus_get_columns;
}


/****************************************************************************/
/* GType and nautilus module stuff */

static GType en_type = 0;

static void epittance_nautilus_instance_init (EPittanceNautilus * epn) {
}

static void epittance_nautilus_class_init (EPittanceNautilusClass * klass) {
	parent_class = g_type_class_peek_parent (klass);
}

static GType epittance_nautilus_get_type (void) {
  return en_type;
}

static void epittance_nautilus_register_type (GTypeModule * module) {
  static const GTypeInfo info = {
    sizeof (EPittanceNautilusClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) epittance_nautilus_class_init,
    NULL,
    NULL,
    sizeof (EPittanceNautilus),
    0,
    (GInstanceInitFunc) epittance_nautilus_instance_init,
  };

  static const GInterfaceInfo column_provider_iface_info = {
    (GInterfaceInitFunc) epittance_nautilus_column_provider_iface_init,
    NULL,
    NULL
  };
  
  static const GInterfaceInfo property_page_provider_iface_info = {
    (GInterfaceInitFunc) epittance_nautilus_property_page_provider_iface_init,
    NULL,
    NULL
  };
  
  static const GInterfaceInfo info_provider_iface_info = {
    (GInterfaceInitFunc) epittance_nautilus_info_provider_iface_init,
    NULL,
    NULL
  };
  
  en_type = g_type_module_register_type (module, 
					 G_TYPE_OBJECT,
					 "EPittanceNautilus",
					 &info, 0);
  
  g_type_module_add_interface (module,
			       en_type,
			       NAUTILUS_TYPE_COLUMN_PROVIDER,
			       &column_provider_iface_info);
  
  g_type_module_add_interface (module,
			       en_type,
			       NAUTILUS_TYPE_INFO_PROVIDER,
			       &info_provider_iface_info);
  
  
  g_type_module_add_interface (module,
			       en_type,
			       NAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER,
			       &property_page_provider_iface_info);
}

static gboolean epittance_nautilus_start_server (gpointer * data) {
  GNOME_EPittance_Main corba_shell;
  CORBA_Environment ev;

  CORBA_exception_init (&ev);

  corba_shell = bonobo_activation_activate_from_id ("OAFIID:GNOME_EPittance_Server",
						    0, NULL, &ev);
  if (ev._major != CORBA_NO_EXCEPTION || corba_shell == CORBA_OBJECT_NIL) {
    g_warning ("Failed to start EPittance Server\n");
  }

  CORBA_exception_free (&ev);

  return FALSE;
}

void nautilus_module_initialize (GTypeModule * module) {
#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  g_idle_add ((GSourceFunc) epittance_nautilus_start_server, NULL);

  epittance_nautilus_register_type (module);
}

/* Perform module-specific shutdown. */
void nautilus_module_shutdown (void) {
}


void nautilus_module_list_types (const GType ** types,
				 int * num_types) {
  static GType type_list[1];
  
  type_list[0] = EPITTANCE_TYPE_NAUTILUS;

  *types = type_list;
  *num_types = 1;
}
