/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "epittance-backend.h"

struct _EPittanceBackendPrivate {
};

static void epittance_backend_class_init (EPittanceBackendClass * klass);
static void epittance_backend_init (EPittanceBackend * backend);
static void epittance_backend_finalize (GObject * object);

static GObjectClass * parent_class;

GType epittance_backend_get_type (void) {
  static GType epittance_backend_type = 0;

  if (!epittance_backend_type) {
    static GTypeInfo info = {
      sizeof (EPittanceBackendClass),
      (GBaseInitFunc) NULL,
      (GBaseFinalizeFunc) NULL,
      (GClassInitFunc) epittance_backend_class_init,
      NULL, NULL,
      sizeof (EPittanceBackend),
      0,
      (GInstanceInitFunc) epittance_backend_init,
    };
    epittance_backend_type = g_type_register_static (G_TYPE_OBJECT,
						     "EPittanceBackend",
						     &info,
						     0);
  }

  return epittance_backend_type;
}

/* Class initialization function for the calendar backend */
static void epittance_backend_class_init (EPittanceBackendClass * klass) {
  GObjectClass * object_class;

  parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

  object_class = (GObjectClass *) klass;

  object_class->finalize = epittance_backend_finalize;

  klass->set_share_status = NULL;
  klass->get_share_status = NULL;

  klass->set_share_name = NULL;
  klass->get_share_name = NULL;

  klass->set_auth_required = NULL;
  klass->get_auth_required = NULL;

  klass->set_password = NULL;
  klass->get_password = NULL;

  klass->add_username = NULL;
  klass->del_username = NULL;
  klass->get_userlist = NULL;
}

/* Object initialization func for the calendar backend */
void epittance_backend_init (EPittanceBackend * backend) {
  EPittanceBackendPrivate * priv;

  priv = g_new0 (EPittanceBackendPrivate, 1);
  backend->priv = priv;
}

void epittance_backend_finalize (GObject * object) {
  EPittanceBackend * backend = (EPittanceBackend *) object;
  EPittanceBackendPrivate * priv;

  priv = backend->priv;

  g_free (priv);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}
