/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef _EPITTANCE_BACKEND_H_
#define _EPITTANCE_BACKEND_H_

#include <glib-object.h>

#include "epittance-main.h"

G_BEGIN_DECLS

#define EPITTANCE_TYPE_BACKEND (epittance_backend_get_type ())
#define EPITTANCE_BACKEND(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), EPITTANCE_TYPE_BACKEND, EPittanceBackend))
#define EPITTANCE_BACKEND_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), EPITTANCE_TYPE_BACKEND, EPittanceBackendClass))
#define EPITTANCE_IS_BACKEND(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EPITTANCE_TYPE_BACKEND))
#define EPITTANCE_IS_BACKEND_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), EPITTANCE_TYPE_BACKEND))

typedef struct _EPittanceBackend EPittanceBackend;
typedef struct _EPittanceBackendClass EPittanceBackendClass;
typedef struct _EPittanceBackendPrivate EPittanceBackendPrivate;

struct _EPittanceBackend {
  GObject parent_object;

  EPittanceBackendPrivate * priv;
};

struct _EPittanceBackendClass {
  GObjectClass parent_class;

  /* Virtual Methods */
  void (* set_share_status) (EPittanceBackend * backend,
			     const gchar * path,
			     const gchar * username,
			     gint status);
  void (* get_share_status) (EPittanceBackend * backend,
			     const gchar * path,
			     const gchar * username);

  void (* set_share_name) (EPittanceBackend * backend,
			   const gchar * path,
			   const gchar * name);
  gchar * (* get_share_name) (EPittanceBackend * backend,
			      const gchar * path);

  void (* set_auth_required) (EPittanceBackend * backend,
			      const gchar * path,
			      const gchar * username,
			      gboolean auth_required);
  gboolean (* get_auth_required) (EPittanceBackend * backend,
				  const gchar * path,
				  const gchar * username);

  void (* set_password) (EPittanceBackend * backend,
			 const gchar * path,
			 const gchar * username,
			 const gchar * password);
  gchar * (* get_password) (EPittanceBackend * backend,
			    const gchar * path,
			    const gchar * username);

  void (* add_username) (EPittanceBackend * backend,
			 const gchar * username);
  void (* del_username) (EPittanceBackend * backend,
			 const gchar * username);
  GList * (* get_userlist) (EPittanceBackend * backend,
			    const gchar * path);
};

GType epittance_backend_get_type (void);

G_END_DECLS

#endif
