/*
 *  epittance-howl.c - EPittance howl integration
 * 
 *  Authors: Christian Kellner <gicmo@gnome-de.org>
 *           Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gi18n-lib.h>

/* Workaround broken howl installing config.h */
#undef PACKAGE
#undef VERSION
#undef PACKAGE_VERSION
#undef PACKAGE_TARNAME
#undef PACKAGE_NAME
#undef PACKAGE_STRING

#include <howl.h>

#include "epittance-howl.h"

struct 	_EPittanceHowlHandle {
  sw_discovery_publish_id id;
};

static sw_discovery session;
static int howl_fd;

static sw_result publish_cb (sw_discovery discovery,
			     sw_discovery_publish_status status,
			     sw_discovery_oid id,
			     sw_opaque extra) {
  return SW_OKAY;
}


static gboolean howl_input (GIOChannel * io_channel,
			    GIOCondition cond,
			    gpointer callback_data) {
  sw_discovery local_session;
  local_session = callback_data;
  sw_salt salt;

  if (sw_discovery_salt (local_session, &salt) == SW_OKAY) {
    sw_salt_lock (salt);
    sw_discovery_read_socket (local_session);
    sw_salt_unlock (salt);
  }
  return TRUE;
}

gboolean epittance_howl_init (void) {
  GIOChannel *channel;

  if (sw_discovery_init (&session) != SW_OKAY)
    return FALSE;
  
  howl_fd = sw_discovery_socket (session);
  if (howl_fd <= 0)
    return FALSE;

  channel = g_io_channel_unix_new (howl_fd);
  
  g_io_add_watch (channel,
		  G_IO_IN,
		  howl_input, session);
  
  g_io_channel_unref (channel);
  
  return TRUE; 
}


gboolean epittance_howl_publish_start (EPittanceHowlHandle ** handle,
				       gint port) {	
  sw_discovery_publish_id id;
  sw_result result;
  char * share_name;
  
  *handle = NULL;
  
  share_name = g_strdup_printf (_("%s's shares"), g_get_user_name ());

  result = sw_discovery_publish (session, 0,
				 share_name,
				 "_webdav._tcp",
				 NULL, NULL,
				 port, "", 0,
				 publish_cb, NULL, &id);
  
  g_free (share_name);
  
  if (result == SW_OKAY) {
    *handle = g_new0 (EPittanceHowlHandle, 1);
    (*handle)->id = id;
  }
  
  return result == SW_OKAY;
}

void epittance_howl_publish_stop (EPittanceHowlHandle * handle) {
  if (handle->id == 0)
    return;
  
  sw_discovery_cancel (session, handle->id);
  
  g_free (handle);
}
