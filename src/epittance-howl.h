/*
 *  Authors: Christian Kellner <gicmo@gnome-de.org>
 *           Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */
 
#ifndef EPITTANCE_HOWL_H
#define EPITTANCE_HOWL_H

typedef struct _EPittanceHowlHandle EPittanceHowlHandle;

gboolean epittance_howl_init (void);
void epittance_howl_publish_stop (EPittanceHowlHandle *handle);
gboolean epittance_howl_publish_start (EPittanceHowlHandle **handle, 
				       gint port);

#endif /* EPITTANCE_HOWL_H */
