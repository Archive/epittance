/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "epittance-main.h"

#define PARENT_TYPE bonobo_object_get_type ()

struct _EPittanceMainPrivate {
  gchar * iid;
};

BONOBO_CLASS_BOILERPLATE_FULL (EPittanceMain, epittance_main,
			       GNOME_EPittance_Main, BonoboObject,
			       BONOBO_OBJECT_TYPE)

static void destroy (BonoboObject * object) {
  EPittanceMain * shell;
  EPittanceMainPrivate * priv;

  shell = EPITTANCE_MAIN (object);
  priv = shell->priv;
  if (priv->iid != NULL) {
    bonobo_activation_active_server_unregister (priv->iid,
						bonobo_object_corba_objref (BONOBO_OBJECT (shell)));
  }
  g_free (priv);
}

static void epittance_main_class_init (EPittanceMainClass * klass) {
  BonoboObjectClass * object_class;
  POA_GNOME_EPittance_Main__epv * epv;

  parent_class = gtk_type_class (PARENT_TYPE);

  object_class = BONOBO_OBJECT_CLASS (klass);

  object_class->destroy = destroy;

  epv = &klass->epv;
}

static void epittance_main_instance_init (EPittanceMain * shell) {
  EPittanceMainPrivate *priv;

  priv = g_new0 (EPittanceMainPrivate, 1);
  priv->iid = NULL;
  shell->priv = priv;
}

static gboolean epittance_main_construct (EPittanceMain * shell,
	const gchar * iid) {
  EPittanceMainPrivate * priv;
  CORBA_Object corba_object;

  g_return_val_if_fail (shell != NULL, FALSE);

  corba_object = bonobo_object_corba_objref (BONOBO_OBJECT (shell));
  if (bonobo_activation_active_server_register (iid, corba_object) !=
      Bonobo_ACTIVATION_REG_SUCCESS) {
    return FALSE;
  }

  while (gtk_events_pending ()) {
    gtk_main_iteration ();
  }

  priv = shell->priv;

  priv->iid = g_strdup (iid);

  return TRUE;
}

EPittanceMain * epittance_main_new (void) {
  EPittanceMain * new;
  EPittanceMainPrivate * priv;

  new = g_object_new (epittance_main_get_type (), NULL);

  if (!epittance_main_construct (new, "OAFIID:GNOME_EPittance_Server")) {
    bonobo_object_unref (BONOBO_OBJECT (new));
    return NULL;
  }

  priv = new->priv;

  return new;
}

