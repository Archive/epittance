/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef _EPITTANCE_MAIN_H_
#define _EPITTANCE_MAIN_H_

#include <bonobo-activation/bonobo-activation.h>
#include <bonobo.h>

#include "EPittance.h"

typedef struct _EPittanceMainPrivate EPittanceMainPrivate;

typedef struct {
  BonoboObject parent;

  EPittanceMainPrivate * priv;
} EPittanceMain;

typedef struct {
  BonoboObjectClass parent_class;

  POA_GNOME_EPittance_Main__epv epv;
} EPittanceMainClass;

#define EPITTANCE_TYPE_MAIN (epittance_main_get_type())
#define EPITTANCE_MAIN(obj) (GTK_CHECK_CAST ((obj), EPITTANCE_TYPE_MAIN, EPittanceMain))
#define EPITTANCE_MAIN_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), EPITTANCE_TYPE_MAIN, EPittanceMainClass))
#define EPITTANCE_IS_MAIN(obj) (GTK_CHECK_TYPE ((obj), EPITTANCE_TYPE_MAIN))
#define EPITTANCE_IS_MAIN_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((obj), EPITTANCE_TYPE_MAIN))

GType epittance_main_get_type (void);
EPittanceMain * epittance_main_new (void);

#endif

