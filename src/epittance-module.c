/*
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *  Authors: Rodney Dawes <dobey@novell.com>
 *           Chris Toshok <toshok@ximian.com>
 *           Dave Camp <dave@ximian.com>
 * 
 */

#include <config.h>
#include "epittance-module.h"

#include <gmodule.h>
#include <libgnome/gnome-macros.h>

#define EPITTANCE_TYPE_MODULE (epittance_module_get_type ())
#define EPITTANCE_MODULE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), EPITTANCE_TYPE_MODULE, EPittanceModule))
#define EPITTANCE_MODULE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), EPITTANCE_TYPE_MODULE, EPittanceModule))
#define EPITTANCE_IS_MODULE(obj) (G_TYPE_INSTANCE_CHECK_TYPE ((obj), EPITTANCE_TYPE_MODULE))
#define EPITTANCE_IS_MODULE_CLASS(klass) (G_TYPE_CLASS_CHECK_CLASS_TYPE ((klass), EPITTANCE_TYPE_MODULE))

typedef struct _EPittanceModule EPittanceModule;
typedef struct _EPittanceModuleClass EPittanceModuleClass;

struct _EPittanceModule {
  GTypeModule parent;

  GModule *library;

  char *path;

  void (*initialize) (GTypeModule  * module);
  void (*shutdown)   (void);

  void (*list_types) (const GType ** types,
		      gint * num_types);

};

struct _EPittanceModuleClass {
  GTypeModuleClass parent;	
};

static GType epittance_module_get_type (void);

static GList * module_objects = NULL;

static void epittance_module_add_type (GType type);

GNOME_CLASS_BOILERPLATE (EPittanceModule,
			 epittance_module,
			 GTypeModule,
			 G_TYPE_TYPE_MODULE);

static gboolean epittance_module_load (GTypeModule * gmodule) {
  EPittanceModule * module;
  
  module = EPITTANCE_MODULE (gmodule);
  
  module->library = g_module_open (module->path, 0);

  if (!module->library) {
    g_warning (g_module_error ());
    return FALSE;
  }

  if (!g_module_symbol (module->library,
			"epittance_module_initialize",
			(gpointer *)&module->initialize) ||
      !g_module_symbol (module->library,
			"epittance_module_shutdown",
			(gpointer *)&module->shutdown) ||
      !g_module_symbol (module->library,
			"epittance_module_list_types",
			(gpointer *)&module->list_types)) {

    g_warning (g_module_error ());
    g_module_close (module->library);
    
    return FALSE;
  }

  module->initialize (gmodule);
  
  return TRUE;
}

static void epittance_module_unload (GTypeModule * gmodule) {
  EPittanceModule * module;
  
  module = EPITTANCE_MODULE (gmodule);
  
  module->shutdown ();
  
  g_module_close (module->library);
  
  module->initialize = NULL;
  module->shutdown = NULL;
  module->list_types = NULL;
}

static void epittance_module_finalize (GObject * object) {
  EPittanceModule * module;
  
  module = EPITTANCE_MODULE (object);

  g_free (module->path);
  
  if (G_OBJECT_CLASS (parent_class)->finalize)
    (*G_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void epittance_module_instance_init (EPittanceModule * module) {
}

static void epittance_module_class_init (EPittanceModuleClass * klass) {
	G_OBJECT_CLASS (klass)->finalize = epittance_module_finalize;
	G_TYPE_MODULE_CLASS (klass)->load = epittance_module_load;
	G_TYPE_MODULE_CLASS (klass)->unload = epittance_module_unload;
}

static void module_object_weak_notify (gpointer user_data, GObject * object) {
  module_objects = g_list_remove (module_objects, object);
}

static void add_module_objects (EPittanceModule * module) {
  const GType * types;
  gint num_types;
  gint i;
  
  module->list_types (&types, &num_types);
  
  for (i = 0; i < num_types; i++) {
    epittance_module_add_type (types[i]);
  }
}

static EPittanceModule * epittance_module_load_file (const gchar * filename) {
  EPittanceModule *module;
  
  module = g_object_new (EPITTANCE_TYPE_MODULE, NULL);
  module->path = g_strdup (filename);
  
  if (g_type_module_use (G_TYPE_MODULE (module))) {
    add_module_objects (module);
    g_type_module_unuse (G_TYPE_MODULE (module));
    return module;
  } else {
    g_object_unref (module);
    return NULL;
  }
}

static void load_module_dir (const gchar * dirname) {
  GDir * dir;
  
  dir = g_dir_open (dirname, 0, NULL);
  
  if (dir) {
    const gchar * name;
    
    while ((name = g_dir_read_name (dir))) {
      if (g_str_has_suffix (name, "." G_MODULE_SUFFIX)) {
	gchar * filename;

	filename = g_build_filename (dirname, 
				     name, 
				     NULL);
	epittance_module_load_file (filename);
      }
    }

    g_dir_close (dir);
  }
}

void epittance_module_init (void) {
  static gboolean initialized = FALSE;

  if (!initialized) {
    initialized = TRUE;
    
    load_module_dir (EPITTANCE_MODULES_DIR);
  }
}

static void epittance_module_add_type (GType type) {
  GObject * object;

  g_message ("adding type `%s'", g_type_name (type));

  object = g_object_new (type, NULL);
  g_object_weak_ref (object, 
		     (GWeakNotify)module_object_weak_notify,
		     NULL);

  module_objects = g_list_prepend (module_objects, object);
}
