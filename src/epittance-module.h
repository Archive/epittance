/*
 * Copyright (C) 2004  Novell, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Rodney Dawes <dobey@novell.com>
 *          Chris Toshok <toshok@ximian.com>
 */

#ifndef _EPITTANCE_MODULE_H
#define _EPITTANCE_MODULE_H

#include <glib-object.h>

G_BEGIN_DECLS

void epittance_module_init (void);

/* The following three functions should exist in modules that are
   written to be dynamically loaded */
void epittance_module_initialize (GTypeModule * module);
void epittance_module_shutdown   (void);
void epittance_module_list_types (const GType ** types, gint * num_types);

G_END_DECLS

#endif /* _EPITTANCE_MODULE_H */
