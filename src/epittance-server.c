/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "epittance-server.h"

static EPittanceMain * shell = NULL;

static void epittance_server_session_died (GnomeClient * client,
					   gpointer * data) {
  bonobo_main_quit ();
}

static gboolean epittance_server_idle_callback (gpointer * data) {
  GNOME_EPittance_Main corba_shell;
  CORBA_Environment ev;

  CORBA_exception_init (&ev);

  shell = epittance_main_new ();
  if (shell == NULL) {
    corba_shell = bonobo_activation_activate_from_id ("OAFIID:GNOME_EPittance_Server",
                  0, NULL, &ev);
    if (ev._major != CORBA_NO_EXCEPTION || corba_shell == CORBA_OBJECT_NIL) {
      CORBA_exception_free (&ev);
      bonobo_main_quit ();
      return FALSE;
    }
  } else {
    corba_shell = bonobo_object_corba_objref (BONOBO_OBJECT (shell));
    corba_shell = CORBA_Object_duplicate (corba_shell, &ev);
    Bonobo_Unknown_ref (corba_shell, &ev);
  }

  CORBA_exception_free (&ev);

  if (shell == NULL) {
    bonobo_main_quit ();
  }
  return FALSE;
}

gint main (gint argc, gchar ** argv) {
  GnomeProgram * program;
  GnomeClient * client;

  program = gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE,
				argc, argv,
				GNOME_PARAM_POPT_TABLE, NULL,
				NULL);

  client = gnome_master_client ();

  g_signal_connect (G_OBJECT (client), "die",
		    G_CALLBACK (epittance_server_session_died), NULL);

  gnome_client_connect (client);
  gnome_client_set_restart_style (client, GNOME_RESTART_IMMEDIATELY);
  gnome_client_set_restart_command (client, argc, argv);

  epittance_module_init ();

  g_idle_add ((GSourceFunc) epittance_server_idle_callback, NULL);
  bonobo_main ();

  return 0;
}
