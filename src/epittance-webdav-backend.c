/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "epittance-backend.h"
#include "epittance-howl.h"
#include "epittance-webdav.h"

typedef struct {
  EPittanceBackend parent_object;

  EPittanceDAVServer * server;
} EPittanceWebDAVBackend;

typedef struct {
  EPittanceBackendClass parent_class;
} EPittanceWebDAVBackendClass;

static EPittanceBackendClass * parent_class;

static void epittance_webdav_backend_init (EPittanceWebDAVBackend * backend);
static void epittance_webdav_backend_class_init (EPittanceWebDAVBackendClass * klass);
static void epittance_webdav_backend_finalize (GObject * object);

static GType epittance_webdav_backend_get_type (GTypeModule * module) {
  GType type;

  GTypeInfo info = {
    sizeof (EPittanceWebDAVBackendClass),
    NULL,
    NULL,
    (GClassInitFunc) epittance_webdav_backend_class_init,
    NULL,
    NULL,
    sizeof (EPittanceBackend),
    0,
    (GInstanceInitFunc) epittance_webdav_backend_init
  };

  type = g_type_module_register_type (module,
				      EPITTANCE_TYPE_BACKEND,
				      "EPittanceWebDAVBackend",
				      &info, 0);

  return type;
}

static void epittance_webdav_backend_init (EPittanceWebDAVBackend * backend) {
  backend->server = g_new0 (EPittanceDAVServer, 1);

  epittance_howl_init ();
  epittance_webdav_server_start (backend->server);
}

static void epittance_webdav_backend_class_init (EPittanceWebDAVBackendClass * klass) {
  GObjectClass * object_class;
  EPittanceBackendClass * backend_class;

  object_class = (GObjectClass *) klass;
  backend_class = (EPittanceBackendClass *) klass;

  parent_class = (EPittanceBackendClass *) g_type_class_peek_parent (klass);

  object_class->finalize = epittance_webdav_backend_finalize;
}

static void epittance_webdav_backend_finalize (GObject * object) {
  EPittanceWebDAVBackend * backend;

  backend = (EPittanceWebDAVBackend *) object;

  gconf_client_remove_dir (backend->server->gconf,
			   "/desktop/gnome/file-sharing", NULL);

  g_object_unref (backend->server->gconf);

  g_free (backend->server);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GType webdav_types[1];

void epittance_module_initialize (GTypeModule * module) {
  webdav_types[0]  = epittance_webdav_backend_get_type (module);
}

void epittance_module_shutdown (void) {
}

void epittance_module_list_types (const GType ** types, gint * num_types) {
  *types = webdav_types;
  *num_types = 1;
}

