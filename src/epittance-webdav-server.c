/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <string.h>

#include "epittance-webdav.h"
#include "epittance-webdav-server.h"

static void epittance_dav_server_path_map_init (EPittanceDAVServer * server);

static void epittance_webdav_folders_changed (GConfClient * client,
					      guint cnxn_id,
					      GConfEntry * entry,
					      EPittanceDAVServer * server) {
  epittance_dav_server_path_map_init (server);
}

static gboolean e_kill_me_now (gpointer key, gpointer value, gpointer data) {
  return TRUE;
}

static void epittance_dav_server_path_map_init (EPittanceDAVServer * server) {
  GSList * folders;

  folders = gconf_client_get_list (server->gconf,
				   "/desktop/gnome/file-sharing/folders",
				   GCONF_VALUE_STRING, NULL);

  if (server->pathmap != NULL) {
    g_hash_table_foreach_remove (server->pathmap, e_kill_me_now, NULL);
  }

  for (; folders != NULL; folders = folders->next) {
    gchar * path, * vpath;

    path = g_strdup (strrchr (folders->data, '/'));
    vpath = g_hash_table_lookup (server->pathmap, path);
    if (vpath == NULL) {
      g_hash_table_insert (server->pathmap, g_strdup (path),
			   g_strdup (folders->data));
    }

    g_free (vpath);
    g_free (path);
  }
}

static void epittance_webdav_request_callback (SoupServerContext * ctx,
					       SoupMessage * message,
					       EPittanceDAVServer * server) {
  gchar * path, * full_path, * share_path;

  path = soup_uri_to_string (soup_message_get_uri (message), TRUE);
  if (!path || *path != '/') {
    soup_message_set_status (message, SOUP_STATUS_BAD_REQUEST);
    g_free (path);

    return;
  }

  switch (soup_method_get_id (message->method)) {
  case SOUP_METHOD_ID_GET:
    full_path = epittance_webdav_server_get_full_path (server, path);
    share_path = g_strdup (g_path_get_dirname (full_path));

    if (g_file_test (full_path, G_FILE_TEST_IS_DIR) ||
	!g_file_test (full_path, G_FILE_TEST_EXISTS)) {
      soup_message_set_status (message, SOUP_STATUS_NOT_FOUND);
    } else {
      if (!epittance_webdav_server_path_is_shared (share_path)) {
	soup_message_set_status (message, SOUP_STATUS_NOT_FOUND);
      } else {
	g_file_get_contents (full_path, &message->response.body,
			     &message->response.length, NULL);
	soup_message_set_status (message, SOUP_STATUS_OK);
      }
    }
    g_free (share_path);
    g_free (full_path);

    break;
  case SOUP_METHOD_ID_PROPFIND:
    epittance_webdav_parse_propfind (server, message, path);
    break;
  case SOUP_METHOD_ID_OPTIONS:
    soup_message_add_header (message->response_headers, "DAV",
			     "1,2");
    soup_message_add_header (message->response_headers, "Allow",
			     "OPTIONS PROPFIND GET");
    soup_message_set_status (message, SOUP_STATUS_OK);
    break;
  default:
    g_print ("Unknown Request Method: %s\n", message->method);
    soup_message_set_status (message, SOUP_STATUS_NOT_IMPLEMENTED);
    break;
  }

  g_free (path);
  soup_server_message_set_encoding (SOUP_SERVER_MESSAGE (message),
				    SOUP_TRANSFER_CONTENT_LENGTH);
}

gboolean epittance_webdav_server_start (EPittanceDAVServer * server) {
  server->gconf = gconf_client_get_default ();

  gconf_client_add_dir (server->gconf, "/desktop/gnome/file-sharing",
			GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
  gconf_client_notify_add (server->gconf,
			   "/desktop/gnome/file-sharing/folders",
			   (GConfClientNotifyFunc) epittance_webdav_folders_changed,
			   server,
			   NULL, NULL);

  server->server = soup_server_new (SOUP_SERVER_PORT,
				    SOUP_ADDRESS_ANY_PORT,
				    NULL);

  if (!server->server) {
    return FALSE;
  }

  server->pathmap = g_hash_table_new (g_str_hash, g_str_equal);
  epittance_dav_server_path_map_init (server);

  soup_server_add_handler (server->server, NULL, NULL,
			   (SoupServerCallbackFn) epittance_webdav_request_callback,
			   NULL, server);

  soup_server_run_async (server->server);

  epittance_howl_publish_start (&(server->howl_handle),
				soup_server_get_port (server->server));
  return FALSE;
}

gchar * epittance_webdav_server_get_full_path (EPittanceDAVServer * server,
					       const gchar * path) {
  gchar * fname;
  gchar * full_path;

  fname = g_path_get_basename (path);

  if (fname != NULL && !strcmp (fname, path + 1)) {
    g_free (fname);
    fname = NULL;
  }

  if (!strcmp (path, "/"))
    full_path = g_strdup (g_get_home_dir ());
  else
    if (fname != NULL)
      full_path = g_strconcat (g_hash_table_lookup (server->pathmap,
						    g_path_get_dirname (path)),
			       "/", fname, NULL);
    else
      full_path = g_strdup (g_hash_table_lookup (server->pathmap, path));

  g_free (fname);

  return full_path;
}

gboolean epittance_webdav_server_path_is_shared (const gchar * path) {
  GConfClient * client;
  GSList * folders;
  gboolean in_list = FALSE;

  client = gconf_client_get_default ();
  folders = gconf_client_get_list (client,
				   "/desktop/gnome/file-sharing/folders",
				   GCONF_VALUE_STRING, NULL);

  for (; folders != NULL; folders = folders->next) {
    if (!strncmp (folders->data, path, strlen (folders->data)))
      in_list = TRUE;
  }

  g_object_unref (client);

  return in_list;
}
