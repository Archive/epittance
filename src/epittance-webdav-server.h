/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *           Christian Kellner <gicmo@gnome-de.org>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */
#ifndef _EPITTANCE_WEBDAV_SERVER_H_
#define _EPITTANCE_WEBDAV_SERVER_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gconf/gconf-client.h>

#include <libsoup/soup.h>
#include <libsoup/soup-address.h>
#include <libsoup/soup-server.h>
#include <libsoup/soup-server-message.h>

typedef struct _EPittanceDAVServer EPittanceDAVServer;

#include "epittance-howl.h"

struct _EPittanceDAVServer {
  SoupServer * server;
  EPittanceHowlHandle * howl_handle;

  GConfClient * gconf;

  GHashTable * pathmap;
};

gboolean epittance_webdav_server_start (EPittanceDAVServer * server);

gchar * epittance_webdav_server_get_full_path (EPittanceDAVServer * server,
					       const gchar * path);
gboolean epittance_webdav_server_path_is_shared (const gchar * path);

#endif
