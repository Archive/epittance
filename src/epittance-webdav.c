/*
 *  Authors: Rodney Dawes <dobey@novell.com>
 *           Christian Kellner <gicmo@gnome-de.org>
 *
 *  Copyright 2004 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License
 *  as published by the Free Software Foundation
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <libgnomevfs/gnome-vfs.h>
#include <string.h>
#include <time.h>

#include "epittance-webdav.h"

extern char * tzname[2];
extern long timezone;
extern int daylight;

void epittance_webdav_parse_propfind (EPittanceDAVServer * server,
				      SoupMessage * message,
				      const gchar * path) {
  xmlDoc * request, * response;
  xmlNode * root, * props, * prop;
  xmlNode * iroot, * rsp, * href, * pstat, * iprop, * item, * status, * rtype;
  xmlNs * ns;
  GnomeVFSFileInfo * info, * ninfo;
  GnomeVFSResult result;
  GnomeVFSDirectoryHandle * dir_handle;
  gchar * full_path, * status_string;

  tzset ();

  xmlKeepBlanksDefault (0);

  full_path = epittance_webdav_server_get_full_path (server, path);
  if (!full_path) {
    soup_message_set_status (message, SOUP_STATUS_NOT_FOUND);
    return;
  }

  if (g_file_test (full_path, G_FILE_TEST_IS_DIR) &&
      strcmp (full_path, "/")) {
    gchar * tmp;

    tmp = g_hash_table_lookup (server->pathmap, path);
    if (tmp == NULL) {
      g_hash_table_insert (server->pathmap, g_strdup (path),
			   g_strdup (full_path));
    }
  }

  request = xmlReadMemory (message->request.body, message->request.length,
			   NULL, NULL,
			   XML_PARSE_NSCLEAN | XML_PARSE_NONET | XML_PARSE_NOWARNING);
  response = xmlNewDoc ("1.0");

  ns = xmlNewNs (NULL, "DAV:", "D");

  root = xmlDocGetRootElement (request);
  iroot = xmlNewNode (ns, "multistatus");
  xmlDocSetRootElement (response, iroot);
  xmlNewProp (iroot, "xmlns:D","DAV:");

  rsp = xmlNewChild (iroot, ns, "response", NULL);
  href = xmlNewTextChild (rsp, ns, "href", path);
  pstat = xmlNewChild (rsp, ns, "propstat", NULL);
  iprop = xmlNewChild (pstat, ns, "prop", NULL);

  info = gnome_vfs_file_info_new ();
  gnome_vfs_get_file_info (gnome_vfs_escape_path_string (full_path), info,
			   GNOME_VFS_FILE_INFO_DEFAULT |
			   GNOME_VFS_FILE_INFO_GET_MIME_TYPE |
			   GNOME_VFS_FILE_INFO_FOLLOW_LINKS);

  for (props = root->children; props != NULL; props = props->next) {
    if (!strcmp (props->name, "prop")) {
      for (prop = props->children; prop != NULL; prop = prop->next) {
	if (!strcmp (prop->name, "text")) {
	} else if (!strcmp (prop->name, "getcontenttype")) {
	  item = xmlNewTextChild (iprop, ns, "getcontenttype",
				  info->mime_type);
	} else if (!strcmp (prop->name, "getcontentlength")) {
	  if (strcmp (info->mime_type, "x-directory/normal")) {
	    gchar * length_str = g_strdup_printf ("%d", (int) info->size);
	    item = xmlNewTextChild (iprop, ns, "getcontentlength",
				    length_str);
	    g_free (length_str);
	  }
	} else if (!strcmp (prop->name, "resourcetype")) {
	  if (!strcmp (info->mime_type, "x-directory/normal")) {
	    item = xmlNewChild (iprop, ns, "resourcetype", NULL);
	    rtype = xmlNewChild (item, ns, "collection", NULL);
	  } else {
	    item = xmlNewChild (iprop, ns, "resourcetype", NULL);
	  }
	} else if (!strcmp (prop->name, "getlastmodified")) {
	  item = xmlNewTextChild (iprop, ns, "getlastmodified",
				  (char *) ctime (&info->mtime));
	}
      }
    }
  }
  status_string = g_strdup_printf ("HTTP/1.%d %d %s",
				   soup_message_get_http_version (message),
				   SOUP_STATUS_OK,
				   soup_status_get_phrase (SOUP_STATUS_OK));
  status = xmlNewTextChild (pstat, NULL, "status", status_string);
  g_free (status_string);

  if (!strcmp (path, "/")) {
    GConfClient * client;
    GSList * folders;

    client = gconf_client_get_default ();
    folders = gconf_client_get_list (client,
				     "/desktop/gnome/file-sharing/folders",
				     GCONF_VALUE_STRING, NULL);

    for (; folders != NULL; folders = folders->next) {
	gchar * filename;

	filename = g_strdup_printf ("%s", (char *) (folders->data +
						    strlen (g_get_home_dir ())));
	rsp = xmlNewChild (iroot, ns, "response", NULL);
	href = xmlNewTextChild (rsp, ns, "href", filename);
	pstat = xmlNewChild (rsp, ns, "propstat", NULL);
	iprop = xmlNewChild (pstat, ns, "prop", NULL);
	
	item = xmlNewTextChild (iprop, ns, "getcontenttype",
				"x-directory/webdav");
	item = xmlNewChild (iprop, ns, "resourcetype", NULL);
	rtype = xmlNewChild (item, ns, "collection", NULL);

	g_free (filename);
    }

    g_object_unref (client);
  } else {
    if (!epittance_webdav_server_path_is_shared (full_path)) {
      g_free (full_path);
      soup_message_set_status (message, SOUP_STATUS_NOT_FOUND);
      return;
    }

    if (info->type == GNOME_VFS_FILE_TYPE_DIRECTORY) {
      result = gnome_vfs_directory_open (&dir_handle, full_path,
					 GNOME_VFS_FILE_INFO_GET_MIME_TYPE);

      ninfo = gnome_vfs_file_info_new ();
      while (gnome_vfs_directory_read_next (dir_handle, ninfo) == GNOME_VFS_OK) {
	gchar * filename;

	filename = g_strdup_printf ("%s/%s", path, ninfo->name);
	rsp = xmlNewChild (iroot, ns, "response", NULL);
	href = xmlNewTextChild (rsp, ns, "href", filename);
	pstat = xmlNewChild (rsp, ns, "propstat", NULL);
	iprop = xmlNewChild (pstat, ns, "prop", NULL);
	
	item = xmlNewTextChild (iprop, ns, "getcontenttype", ninfo->mime_type);
	item = xmlNewTextChild (iprop, ns, "getlastmodified",
				(char *) ctime (&ninfo->mtime));
	if (strcmp (ninfo->mime_type, "x-directory/normal") != 0) {
	  gchar * length_str = g_strdup_printf ("%d", (int) ninfo->size);
	  item = xmlNewTextChild (iprop, ns, "getcontentlength", length_str);
	  g_free (length_str);
	  item = xmlNewChild (iprop, ns, "resourcetype", NULL);
	} else {
	  item = xmlNewChild (iprop, ns, "resourcetype", NULL);
	  rtype = xmlNewChild (item, ns, "collection", NULL);
	}

	g_free (filename);
      }
      gnome_vfs_file_info_unref (ninfo);
    }
  }

  gnome_vfs_file_info_unref (info);
  g_free (full_path);
  xmlFreeDoc (request);

  xmlDocDumpFormatMemory (response, (xmlChar **) &message->response.body,
			  &message->response.length, 1);

  xmlFreeDoc (response);

  soup_message_add_header (message->response_headers, "Content-type",
			   "text/xml");
  soup_message_add_header (message->response_headers, "DAV", "1,2");
  soup_message_set_status (message, SOUP_STATUS_MULTI_STATUS);
}

